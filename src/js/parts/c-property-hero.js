//$(document).ready(function () {
    import $ from "jquery"; 
    const button = document.getElementById('property-hero-lightbox-button');
    button.addEventListener('click', () => {
        let lightbox_items = [];
        
        $('.js-property-hero-lightbox-images').each(function() {
            let url = $(this).data('url');
            let caption = $(this).data('caption');
            lightbox_items.push({
                source: url,
                caption: caption
            });
        });

        const panel = UIkit.lightboxPanel({
            items: lightbox_items
        });

        panel.show()
    });
//});